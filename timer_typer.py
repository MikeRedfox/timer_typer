#!/usr/bin/python
import time
import subprocess
import typer

app = typer.Typer()


@app.command()
def timer(hours: str, mins: str, secs: str):
    """
    Simple timer that notifies you when time is up
    """

    tempo = 0

    tempo += int(hours) * 3600
    tempo += int(mins) * 60
    tempo += int(secs)

    while tempo > 0:
        tempo -= 1
        time.sleep(1)

    subprocess.run(["dunstify", "Timer Scaduto"])

    orol = """

$$$$$$$$\ $$\                       $$\                                   
\__$$  __|\__|                      $  |                                  
   $$ |   $$\ $$$$$$\$$$$\   $$$$$$\\_/$$$$$$$\       $$\   $$\  $$$$$$\  
   $$ |   $$ |$$  _$$  _$$\ $$  __$$\ $$  _____|      $$ |  $$ |$$  __$$\ 
   $$ |   $$ |$$ / $$ / $$ |$$$$$$$$ |\$$$$$$\        $$ |  $$ |$$ /  $$ |
   $$ |   $$ |$$ | $$ | $$ |$$   ____| \____$$\       $$ |  $$ |$$ |  $$ |
   $$ |   $$ |$$ | $$ | $$ |\$$$$$$$\ $$$$$$$  |      \$$$$$$  |$$$$$$$  |
   \__|   \__|\__| \__| \__| \_______|\_______/        \______/ $$  ____/ 
                                                                $$ |      
                                                                $$ |      
                                                                \__|

    """

    print(orol)


if __name__ == "__main__":
    typer.run(timer)
